A bot that posts lunchtime discussion prompts in #lounge on UVic Law's chat server.

Contributing Prompts
-----

Prompts are stored in the folder called 'prompts' and can take a few different formats. The simplest is plain text - create a file with a name ending in .txt, and the bot will post the message inside at some point.

You can also upload single files that aren't text, and they'll be uploaded with no caption.

If you want to post a message with an attachment, create a folder. Inside that folder, place the file you want to upload and a file called 'message.txt' that contains the caption.