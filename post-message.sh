#!/bin/bash

set -e

export CURLOPT_FAILONERROR=1

ROCKETCHAT_URL='https://chat.uviclaw.net'
ROCKETCHAT_ROOM_ID='GENERAL'
ROCKETCHAT_CHANNEL="#lounge"
ROCKETCHAT_USER='lunchbot'
PROMPTS_DIR='prompts'
POSTED_MESSAGES='message-history.txt'

source .secrets


AUTH=$(curl --silent --show-error \
            --header "Content-Type: application/json" \
            ${ROCKETCHAT_URL}/api/v1/login \
            --data "{ \"user\": \"$ROCKETCHAT_USER\", \"password\": \"$ROCKETCHAT_PASSWORD\" }")

USER_ID=$(echo "$AUTH" | jq --raw-output '.data.userId')
AUTH_TOKEN=$(echo "$AUTH" | jq --raw-output '.data.authToken')

if [ ! -f "$POSTED_MESSAGES" ]; then
    touch "$POSTED_MESSAGES"
fi

PROMPT_JSON=$(python3 select_message.py "$PROMPTS_DIR" "$POSTED_MESSAGES" "$ROCKETCHAT_CHANNEL")

MESSAGE_TYPE=$(echo "$PROMPT_JSON" | jq --raw-output '.type')

case $MESSAGE_TYPE in
    'text')
        MESSAGE_BODY=$(echo "$PROMPT_JSON" | jq --raw-output '.body')
        curl --silent --show-error \
             --header "X-Auth-Token: $AUTH_TOKEN" \
             --header "X-User-Id: $USER_ID" \
             --header "Content-Type: application/json" \
             "${ROCKETCHAT_URL}/api/v1/chat.postMessage" \
             --data "$MESSAGE_BODY"
    ;;
    'file')
        FILE_PATH=$(echo "$PROMPT_JSON" | jq --raw-output '.path')
        MIME_TYPE=$(echo "$PROMPT_JSON" | jq --raw-output '.mimetype')
        curl --silent --show-error \
             "${ROCKETCHAT_URL}/api/v1/rooms.upload/${ROCKETCHAT_ROOM_ID}" \
             --form "file=@$FILE_PATH;type=$MIME_TYPE" \
             --header "X-Auth-Token: $AUTH_TOKEN" \
             --header "X-User-Id: $USER_ID"
    ;;
    'text_with_attachment')
        FILE_PATH=$(echo "$PROMPT_JSON" | jq --raw-output '.path')
        MIME_TYPE=$(echo "$PROMPT_JSON" | jq --raw-output '.mimetype')
        MSG=$(echo "$PROMPT_JSON" | jq --raw-output '.msg')
        curl --silent --show-error \
             "${ROCKETCHAT_URL}/api/v1/rooms.upload/${ROCKETCHAT_ROOM_ID}" \
             --form "file=@$FILE_PATH;type=$MIME_TYPE" \
             --form "msg=$MSG" \
             --header "X-Auth-Token: $AUTH_TOKEN" \
             --header "X-User-Id: $USER_ID"
    ;;
esac

echo "$PROMPT_JSON" | jq --raw-output '.key' >> "$POSTED_MESSAGES"
