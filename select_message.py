import argparse
import json
import mimetypes
import os
import random
import sys


class Attachment:
    def __init__(self, path):
        self.path = path
        self.mime_type, _ = mimetypes.guess_type(path)

class TextMessage:
    def __init__(self, channel, text_file_path):
        self.channel = channel
        with open(text_file_path) as f:
            self.text = f.read().rstrip()

    def as_rocketchat_jsonable(self):
        return {
            'type': 'text',
            'body': {
                'channel': self.channel,
                'text': f"Today's lunchtime discussion prompt is: {self.text}"
            }
        }

class FileMessage:
    def __init__(self, path):
        self.attachment = Attachment(path)

    def as_rocketchat_jsonable(self):
        return {
            'type': 'file',
            'path': self.attachment.path,
            'mimetype': self.attachment.mime_type
        }

class DirectoryMessage:
    def __init__(self, dir_path):
        children = os.listdir(dir_path)
        if len([p for p in children if p != 'message.txt']) > 1:
            raise ValueError(f"{dir_path} contains more than one attachment.")

        for child_path in children:
            full_path = os.path.join(dir_path, child_path)
            if not os.path.isfile(full_path):
                raise IsADirectoryError(f"{dir_path} is not flat, contains subdirectory {child_path}")

            if child_path == 'message.txt':
                with open(full_path) as f:
                    self.message = f.read().rstrip();
            else:
                self.attachment = Attachment(full_path)

    def as_rocketchat_jsonable(self):
        return {
            'type': 'text_with_attachment',
            'path': self.attachment.path,
            'mimetype': self.attachment.mime_type,
            'msg': f"Today's lunchtime discussion prompt is: {self.message}"
        }

def main(prompts_dir, history_file_path, channel, print_remaining):

    all_prompts = {}
    for child_path in os.listdir(prompts_dir):
        full_path = os.path.join(prompts_dir, child_path)
        if not os.path.isfile(full_path):
            all_prompts[child_path] = DirectoryMessage(full_path)
        elif os.path.splitext(full_path)[1] == '.txt':
            all_prompts[child_path] = TextMessage(channel, full_path)
        else:
            all_prompts[child_path] = FileMessage(full_path)

    with open(history_file_path) as f:
        history_entries = {line.rstrip() for line in f.readlines()}

    unused_prompts = {key: prompt for key, prompt in all_prompts.items() if key not in history_entries}

    if print_remaining:
        return str(list(unused_prompts.keys()))

    key, prompt = random.choice(list(unused_prompts.items()))

    out = prompt.as_rocketchat_jsonable()
    out['key'] = key

    return json.dumps(out)

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('prompts_dir', type=str)
    arg_parser.add_argument('history_file', type=str)
    arg_parser.add_argument('rocketchat_channel', type=str)
    arg_parser.add_argument('print_remaining', type=bool, nargs='?', default=False)
    args = arg_parser.parse_args()

    sys.stdout.write(main(args.prompts_dir, args.history_file, args.rocketchat_channel, args.print_remaining))
    sys.exit(0)
